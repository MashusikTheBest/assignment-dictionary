%define BUFF 255
%define SIZE 8



section .rodata
msg: db "No such word", 10, 0  
msg2: db "read error", 10, 0
%include "words.inc"

section .text

%include "lib.inc"

global _start

_start:
	push rbp;
    mov rbp, rsp
	sub rsp, BUFF ; делаем буфер
	mov rdi, rsp ; кладем в аргумент адрес буфера
	mov rsi, BUFF ; кладем размер буфера
	push rdi
	push rsi
	call read_word_fix ; читаем слово в буфер
	pop rsi
	pop rdi
	cmp rax, 0 ; если слово больше буфера, то
	je .overflow ; то переходим на overflow
	mov rdi, rax ; кладем адрес слова в аргумент 
	mov rsi, last_elem; кладем адрес начала словаря в другой аргумент
	push rdi
	push rsi 
	call find_word ; ищем слово
	pop rsi
	pop rdi
    push rax
    push rdi
    push rsi
    call print_newline
    pop rsi
    pop rdi
    pop rax
	cmp rax, 0 ; если слова нет, то
	je .not_found
	add rax, SIZE ; пропускаем метку
	push r12 
	mov r12, rax
	mov rdi, rax
	push rdi
	push rsi
	call string_length ; считаем длину ключа
	pop rsi
	pop rdi
    inc rax
	add rax, r12 ; пропускаем и ключ, и метку
	pop r12
	mov rdi, rax
	call print_string
	mov rsp, rbp
	pop rbp
	mov rdi, EXIT_CODE_OK
    jmp exit
.not_found:
	mov rdi, msg
	call error_found
    jmp .end
.overflow:
    mov rdi, msg2
	call error_found
    jmp .end
.end:
    mov rsp, rbp
    pop rbp
    mov rdi, EXIT_CODE_ERROR
    call exit
